"""
This is a flask app for UI on retail sales project
"""
__author__ = "Shlok Chaudhari"


import re
import sys
import logging

import requests as req
import simplejson
from flask import request
from datetime import datetime
from flask import Flask, render_template
from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

logger = XprLogger("ui", level=logging.INFO)


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route('/')
def form_page():
    """
    UI page for Retail sales project
    """

    logger.info("Received request from {}".format(request.remote_addr))
    return render_template('form.html')


@app.route('/result', methods=['POST'])
def result_pred():
    """
    Result prediction response
    """

    logger.info('Receiving request for the HTML page')
    result = request.get_json()

    logger.info('Converting feature values in request to float.'
                'Skipping Date.')
    input_json = {'input': result["input"]}
    for key in input_json['input'].keys():
        if key != 'Date':
            try:
                input_json['input'][key] = float(
                    input_json['input'][key]
                )
            except ValueError:
                if input_json['input'][key] == 'NA':
                    continue
                return {'error': 'Invalid input! Specify numbers'}

    logger.info('Converting Date to Day, Month and Year.')
    if input_json['input']['Date'] == '' and result["Model"] == 'DNN':
        return {'error': 'Date not specified'}
    elif input_json['input']['Date'] != '' and result["Model"] == 'DNN':
        date = datetime.strptime(input_json['input']['Date'], "%Y-%m-%d")
        input_json['input']['Day'] = int(date.day) - 1
        input_json['input']['Month'] = int(date.month) - 1
        input_json['input']['Year'] = int(date.year) - 2013

    logger.info('Extracting model from UI request.')
    model = None
    for key in app.config.keys():
        if re.search(key, result['Model'].lower()):
            model = key
            break
    if model is None:
        return {"error": "Model not specified."}

    logger.info("Sending request to appropriate model's "
                "inference service mesh.")
    url = app.config[model]
    response = req.post(url=url, json=input_json)
    try:
        response = response.json()
    except simplejson.errors.JSONDecodeError:
        return {'error': "Invalid feature model combination"}

    logger.info('Rounding off the result from response to 2 decimal'
                'places')
    response['results'][0] = round(response['results'][0], 2)
    return response


if __name__ == '__main__':
    """
        File that runs a flask API for the Retail sales UI.
        Takes in command line arguments as follows;
            python /path/to/this/file.py model_name model_mesh_link
        Multiple links for multiple models is handled.
    """
    args = sys.argv[1:]
    for i in range(len(args)):
        if not re.search('http', args[i]):
            app.config[args[i]] = args[i+1]
    app.run(host="0.0.0.0", port=8090)
