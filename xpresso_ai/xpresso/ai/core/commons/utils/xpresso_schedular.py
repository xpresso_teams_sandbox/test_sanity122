"""Importing schedule and logging modules"""
import logging
import schedule
import time


class XpressoSchedular:
    """A scheduling class with a method that can execute any function on every interval"""

    def process_periodically(self, function_name):
        """schedules function(function_name) after interval(interval_time)"""
        try:
            # schedule.every().day.do(function_name)
            schedule.every(10).minutes.do(function_name)
            return True

        except ImportError as err:
            logging.error(err)
            raise err

        except AttributeError as err:
            logging.error(err)
            raise err
